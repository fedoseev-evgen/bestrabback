var config = {};

//Статика которая отвечает за стандартные настройки
config.port = 80;
config.name = "Женя бест";
config.home = ""

config.mongoose = {
    uri: "mongodb://127.0.0.1/superblog",
    options: {
        server: {
            socketOptions: {
                keepAlive: 1
            }
        }
    }
};


module.exports=config;